console.log("Konnichiwa!!");

/*	Selection Control Structures
		- allows us to select between two or more alternate paths. 
		- described as either ...

	conditional statements 
		- may conditions ...
			**programming also needs to execute desicion-making tasks that depends ...
		- with this, we can code predetermined actions based on user input and in turn make our code more flexible
*/

/*	What is Conditional Statements?
		- allow us to control the flow of our program.
		- it allows us to run statement/instruction if a condition is met or run another separate instruction if not otherwise.
*/

// [SECTION] IF, ELSE IF and ELSE Statement

/*	IF Statement
		- it will execute the statement/code blocks if a specified consition is met/true.

		Syntax:
		if(condition){
			statement;
		}

		- the result of the expression added in the IF's condition must result to TRUE, else the statement inside the if() will not run.
*/
	let numA = -1;
	if(numA<0){
		console.log("Hello from numA.");
	}
	console.log(numA<0); //true

	// reassign the variable numA and run an IF statement with the same condition.
	numA = 1
	if(numA<0){
		console.log("Hello from numA.");
	}
	console.log(numA<0); //false, it will not run beause the expression now results to false.


	let city = "New York";
	if(city === "New York"){
		console.log("Welcome to New York!");
	}

/*	ELSE IF Statement
		- executes statement if previous conditions are false and if the specified condition is true
		- The "else if" is optional and can be added to capture additional conditions to change the flow of the program.
		- dependent on the IF statement and cannot run alone.

		Syntax:
		if(condition){
			statement;
		}
		else if(anotherCondition){
			elseIfStatement;	
		}

		- we are able to evaluate the ELSE IF() statement after we evaluated that the IF condition was failed

		-If the IF() condition was passed and run, we will no longer evaluate to ELSE IF() and end the process.
*/

	let numH = 1;

	if(numH<0){
		console.log("Hello from (numH<0)");
	}

	else if(numH>0){
		console.log("Hello from (numH>0)");
	}

	/*	{ //code block ({}), para di akalain ng program na kadugtong ito
			else if(numH>0){
			console.log("Hello from (numH>0)");
			}
		} */

	// numH = 1
	if(numH !== 1){ // false
		console.log("Hello from (numH===1)");
	}
	else if(numH<0){ //false
		console.log("Hello from (numH=1)");
	}
	else if(numH>0){ //true, so this will run
		console.log("Hello from the second else if clause!");
	}
	else if(numH===1){ //true, but will not run, coz it com
		console.log("Hello from the third!");
	}

	//reassign city to Tokyo
	city = "New York";
	if(city === "New York"){
		console.log("Welcome to New York!");
	}
	else if(city === "Tokyo"){
		console.log("Welcome to Tokyo!");
	}

/*	ELSE Statement
		- executes statement if all other conditions are false/not met
		- ELSE statement is optional and can be added to capture any other result to change the flow of program.
		- if all of the preceding IF and ELSE IF are not met, the ELSE statement will be executed instead.
		-also dependent with IF statement, it cannot run alone.

		Syntax:
		else{
			elseStatement;
		}
*/
	// reassign numH
	numH = 2;
	if(numH<0){ //false
		console.log("Hello from IF Statement");
	}
	else if(numH>2){ //false
		console.log("Hello from ELSE IF Statement");
	}
	else if(numH>3){ //false
		console.log("Hello from second ELSE IF");
	}
	else{ //true, since all the conditions above are not met, else statement will run
		console.log("Hello from ELSE Statement");
	}

// if, else if, and else staement with function
	
/*	
	Most of the times we would like to use of, else if and else statement with functions to control the flow of our application.
*/

	let message;

	function determineTyphoonIntensity(windSpeed){
		if(windSpeed<0){
			return "Invalid Argument!";
		}
		else if(windSpeed<=30){
			return "Not a Typhoon yet!"
		}
		else if(windSpeed <= 60){
			return "Tropical Depression Detected!";
		}

	/* MINI ACTIVITY
		1. add return "Tropical Storm Detected" if the windspeed is between 60 and 89.
		2. add return Severe "Tropical Storm Detected" if the windspeed is between 88 and 118. 
		3. if higher than 117, return "Typhoon Detected."
	*/

		else if(windSpeed < 90 ){
			return "Tropical Storm Detected!";
		}
		else if(windSpeed <= 117){
			return "Severe Tropical Storm Detected!";
		}
		else if(windSpeed >= 118){
			return "Typhoon Detected!";
		}
		else{
			return "Invalid Argument!";
		}
	}

	// console.log(determineTyphoonIntensity("120"));

	message = determineTyphoonIntensity(119);

	if(message === "Typhoon Detected!"){
		// console.warn()  is a good way to print warning in our console that could help developers act on certain output within our code.
		console.warn(message);
	}

// [SECTION] Truthy or Falsy
/*	
	- JavaScript a "Truthy" is a value that is considered true when encountered in Boolean context
	- Values are considered true unless defined otherwise.

	FALSY VALUES
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN - not a Number
*/

	if(null){
		console.log("Hello from the null inside the if statement");
	}
	else{
		console.log("Hello from the null outside the if statement");
	}

// [SECTION] Condition Operator/ Ternary Operator
/*	
	The Conditional Operator
	1. condition/expression
	2. statement/expression to execute if the condition is true or truty
	3. expression if the condition is falsy

	Syntax:
	(expression) ? ifTrue : ifFalse;
*/

	let ternaryResult = (1>18) ? true : false;
	console.log(ternaryResult);

// [SECTION] Switch Statement
/*
	- evaluates an expression and matches the expression's value to a case class.
	- case sensitive

*/

	// let day = prompt("What day of the week today?").toLowerCase();
	// // tolowercase method will convert all the letters into lowercase.

	// switch(day){
	// 	case 'monday':
	// 		console.log("The color of the day is red!");
	// 		//it means the code will stop here.
	// 		break;
	// 	case 'tuesday':
	// 		console.log("The color of the day is orange!");
	// 		break;
	// 	case 'wednesday':
	// 		console.log("The color of the day is yellow!");
	// 		break;
	// 	case 'thursday':
	// 		console.log("The color of the day is green!");
	// 		break;
	// 	case 'friday':
	// 		console.log("The color of the day is blue!");
	// 		break;
	// 	case 'saturday':
	// 		console.log("The color of the day is indigo!");
	// 		break;
	// 	case 'sunday':
	// 		console.log("The color of the day is violet!");
	// 		break;

	// 	default: //same as else statement
	// 		console.log("Please input a valid day");
	// 		break;
	// }

// [SECTION] Try-Catch-Finally
/*
	Try-Catch Statement
	- for error handling
	- there are instances when the application returns an error/warning that is not necessarily an error in the context of our code.
	- these errors are result of an attempt of the programming language to help developers in creating efficient code.
*/

	function showIntensity(windSpeed){
		try{
			// codes that will be executed or run
			alerat(determineTyphoonIntensity(windSpeed));
			alert("Intensity updates will show alert from try.");
		}
		catch(error){
			console.warn(error.message);
		}
		finally{
			// continue execution of code regardless of success and failure of code execution in the try statement.
			alert("Intensity updates will show new alert from finally.");
		}
	}
	showIntensity(119);